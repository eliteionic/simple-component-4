import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-motivator',
  templateUrl: './motivator.component.html',
  styleUrls: ['./motivator.component.scss']
})
export class MotivatorComponent implements OnInit {

  public quotes: string[] = [];
  public currentQuote: string = '';

  constructor() {
  }

  ngOnInit() {

		this.quotes = [
			"The worst thing I can be is the same as everybody else. - Arnold Schwarzenegger",
			"Strength does not come from winning. Your struggles develop your strengths. - Arnold Schwarzenegger",
			"The pessimist sees difficulty in every opportunity. The optimist sees the opportunity in every difficulty. - Winston Churchill ",
			"The pessimist sees difficulty in every opportunity. The optimist sees the opportunity in every difficulty. - Winston Churchill ",
			"We choose to go to the moon in this decade and do the other things, not because they are easy, but because they are hard. - John F. Kennedy",
			"Don't let yesterday take up too much of today. - Will Rogers",
			"Creativity is intelligence having fun. - Albert Einstein",
			"Do what you can, with all you have, wherever you are. - Theodore Roosevelt"
    ];
    
    this.setRandomQuote();

  }

  setRandomQuote(){

    let randomIndex = Math.floor(Math.random() * this.quotes.length);
    this.currentQuote = this.quotes[randomIndex];

  }

}
